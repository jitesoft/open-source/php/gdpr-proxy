<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCompanyFromUser extends Migration {

    public function up() {
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('user_company_foreign');
            $table->dropColumn('company_id');
        });
    }

    public function down() {
        Schema::table('users', function(Blueprint $table) {
            $table->integer('company_id', false, true)->after('id')->default(0);
            $table->foreign('company_id', 'user_company_foreign')->references('id')->on('companies');
        });
    }
}
