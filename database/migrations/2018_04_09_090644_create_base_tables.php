<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {


        Schema::create('companies', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id', false, true);
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->timestamps();

            $table->foreign('company_id', 'user_company_foreign')->references('id')->on('companies');
        });

        Schema::create('endpoints', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id', false, true);
            $table->text('endpoint');

            $table->timestamps();
            $table->foreign('company_id', 'endpoint_company_foreign')->references('id')->on('companies');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('endpoints', function(Blueprint $table) {
            $table->dropForeign('endpoint_company_foreign');
            $table->drop();
        });

        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('user_company_foreign');
            $table->drop();
        });

        Schema::drop('companies');
    }
}
