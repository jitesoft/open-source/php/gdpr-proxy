<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCustomerAddAuth extends Migration {

    public function up() {
        Schema::table('companies', function(Blueprint $table) {
            $table->string('auth_type')->after('name')->default('');
            $table->json('auth_data')->after('auth_type')->default('');
        });
    }

    public function down() {
        Schema::table('companies', function(Blueprint $table) {
            $table->dropColumn('auth_type');
        });

        Schema::table('companies', function(Blueprint $table) {
            $table->dropColumn('auth_data');
        });
    }
}
