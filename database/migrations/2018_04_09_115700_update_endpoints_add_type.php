<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEndpointsAddType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('endpoints', function(Blueprint $table) {
            $table->string('type')->after('endpoint')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('endpoints', function(Blueprint $table) {
            $table->removeColumn('type');
        });
    }
}
