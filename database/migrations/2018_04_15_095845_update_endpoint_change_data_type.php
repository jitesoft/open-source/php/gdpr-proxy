<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEndpointChangeDataType extends Migration {

    public function up() {
        Schema::table('endpoints', function(Blueprint $table) {
            $table->dropColumn('auth_data');
        });

        Schema::table('endpoints', function(Blueprint $table) {
            $table->text('auth_data')->after('auth_type')->default('');
        });
    }

    public function down() {
        Schema::table('endpoints', function(Blueprint $table) {
            $table->dropColumn('auth_data');
        });
        Schema::table('endpoints', function(Blueprint $table) {
            $table->json('auth_data')->after('auth_type')->default('{}');
        });
    }
}
