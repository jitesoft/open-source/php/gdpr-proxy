<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAuthDataToText extends Migration {

    public function up() {
        Schema::table('companies', function(Blueprint $table) {
            $table->dropColumn('auth_data');
        });

        Schema::table('companies', function(Blueprint $table) {
            $table->text('auth_data')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('companies', function(Blueprint $table) {
            $table->dropColumn('auth_data');
        });

        Schema::table('companies', function(Blueprint $table) {
            $table->json('auth_data')->default('{}');
        });
    }

}
