<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEndpointsAddAuth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('endpoints', function(Blueprint $table) {
            $table->string('auth_type')->after('type')->default('');
            $table->json('auth_data')->after('auth_type')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('endpoints', function(Blueprint $table) {
            $table->removeColumn('auth_type');
            $table->removeColumn('auth_data');
        });
    }

}
