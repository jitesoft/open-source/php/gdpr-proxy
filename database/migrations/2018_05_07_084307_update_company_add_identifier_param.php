<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanyAddIdentifierParam extends Migration {

    public function up() {
        Schema::table('companies', function(Blueprint $table) {
            $table->string('identifier_param')->after('auth_type')->default('identifier');
        });
    }

    public function down() {
        Schema::table('companies', function(Blueprint $table) {
            $table->dropColumn('identifier_param');
        });
    }

}
