<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJobLogs extends Migration {

    public function up() {
        Schema::create('job_logs', function(Blueprint $table) {
            $table->increments('id');
            $table->text('job_data')->comment('Encrypted job data.');
            $table->string('result');
            $table->text('reason')->comment('Encrypted reason message.');

            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('job_logs');
    }
}
