# GDPR - PROXY

[![pipeline status](https://gitlab.com/jitesoft/open-source/php/gdpr-proxy/badges/master/pipeline.svg)](https://gitlab.com/jitesoft/open-source/php/gdpr-proxy/commits/master)
[![coverage report](https://gitlab.com/jitesoft/open-source/php/gdpr-proxy/badges/master/coverage.svg)](https://gitlab.com/jitesoft/open-source/php/gdpr-proxy/commits/master)

A small api written using the lumen framework making it possible to set up multiple endpoints for fetching or removing
data from.  
Each endpoint is bound to a company/consumer which can query the API via exposed endpoints using a given type of authentication.
When querying the api, it creates jobs to either remove or fetch data of a given user identifier (email address or similar).


## Api Authentication

There are two parts of the api, the administration part and the external endpoints for consumers.  
The admin layer uses the lumen passport project to allow for OAuth2 authentication, while the consumer
endpoints uses pre defined authentication methods.

Currently implemented authentication methods for consumers and endpoints:

 * HMAC signature
 * BasicAuth
 
It's possible to add new authentication methods by implementing the `AuthenticationMethodInterface`.

## Documentation

Documentation for setting up the project on your own environment will soon be available here.  
The API documentation is currently available in the `docs/index.html` file and will soon be added to the wiki and possible a page of it own.

## Bugs and feature requests

Submit any feature requests or bug reports to the issue tracker here on gitlab.  
Mark any security vulnerabilities with the `security` tag and they will be highest prio.

## Contributions

Follow the given standard and include tests. Pull/Merge requests might be accepted.  
Only pgp signed commits will be accepted.
