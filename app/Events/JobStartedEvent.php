<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  JobStartedEvent.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Events;

use Jitesoft\GdprProxy\Models\Company;

/**
 * JobStartedEvent
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class JobStartedEvent extends Event {

    protected $requester;
    protected $jobType;

    public function __construct(Company $requester, string $jobType) {
        parent::__construct();

        $this->requester = $requester;
        $this->jobType   = $jobType;
    }

    /**
     * @return Company
     */
    public function getRequester(): Company {
        return $this->requester;
    }

    /**
     * @return string
     */
    public function getJobType(): string {
        return $this->jobType;
    }

}
