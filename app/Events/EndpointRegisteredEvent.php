<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  EndpointRegisteredEvent.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Events;

use Jitesoft\GdprProxy\Models\Endpoint;

/**
 * EndpointRegisteredEvent
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class EndpointRegisteredEvent extends Event {

    private $endpoint;

    public function __construct(Endpoint $endpoint) {
        parent::__construct();

        $this->endpoint = $endpoint;
    }

    /**
     * @return Endpoint
     */
    public function getEndpoint(): Endpoint {
        return $this->endpoint;
    }

}
