<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  CompanyRegisteredEvent.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Events;

use Jitesoft\GdprProxy\Models\Company;

/**
 * CompanyRegisteredEvent
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class CompanyRegisteredEvent extends Event {

    private $company;

    public function __construct(Company $company) {
        parent::__construct();

        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company {
        return $this->company;
    }

}
