<?php
namespace Jitesoft\GdprProxy\Events;

use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;

abstract class Event {
    use SerializesModels;

    protected $eventCreationTime;

    public function getCreationTime(): Carbon {
        return $this->eventCreationTime;
    }

    public function __construct() {
        $this->eventCreationTime = Carbon::now();
    }

}
