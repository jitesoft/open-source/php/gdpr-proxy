<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  UserFetchRequestEvent.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Events;

use Jitesoft\GdprProxy\Models\Company;

/**
 * UserFetchRequestEvent
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class UserFetchRequestEvent extends Event {

    private $requester;

    public function __construct(Company $requester) {
        parent::__construct();

        $this->requester = $requester;
    }

    /**
     * @return Company
     */
    public function getRequester(): Company {
        return $this->requester;
    }

}
