<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  CompanyRemovedEvent.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Events;

/**
 * CompanyRemovedEvent
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class CompanyRemovedEvent extends Event {

    private $companyName;
    private $companyId;

    public function __construct(string $companyName, int $companyId) {
        parent::__construct();

        $this->companyId   = $companyId;
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string {
        return $this->companyName;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int {
        return $this->companyId;
    }

}
