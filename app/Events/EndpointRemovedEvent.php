<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  EndpointRemovedEvent.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Events;

use Jitesoft\GdprProxy\Models\Company;

/**
 * EndpointRemovedEvent
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class EndpointRemovedEvent extends Event {

    protected $company;
    protected $endpoint;
    protected $endpointId;

    public function __construct(Company $company, string $endpoint, int $endpointId) {
        parent::__construct();

        $this->company    = $company;
        $this->endpoint   = $endpoint;
        $this->endpointId = $endpointId;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company {
        return $this->company;
    }

    /**
     * @return string
     */
    public function getEndpoint(): string {
        return $this->endpoint;
    }

    /**
     * @return int
     */
    public function getEndpointId(): int {
        return $this->endpointId;
    }

}
