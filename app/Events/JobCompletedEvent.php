<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  JobCompletedEvent.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Events;

use Jitesoft\GdprProxy\Models\Company;

/**
 * JobCompletedEvent
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class JobCompletedEvent extends Event {

    protected $requester;
    protected $jobType;
    protected $success;

    public function __construct(Company $requester, string $jobType, bool $success) {
        parent::__construct();

        $this->requester = $requester;
        $this->jobType   = $jobType;
        $this->success   = $success;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool {
        return $this->success;
    }

    /**
     * @return Company
     */
    public function getRequester(): Company {
        return $this->requester;
    }

    /**
     * @return string
     */
    public function getJobType(): string {
        return $this->jobType;
    }

}
