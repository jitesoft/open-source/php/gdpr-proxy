<?php
namespace Jitesoft\GdprProxy\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Jitesoft\GdprProxy\Contracts\ConsumerServiceInterface;
use Jitesoft\GdprProxy\Services\ConsumerService;
use Dusterio\LumenPassport\LumenPassport;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        LumenPassport::routes($this->app, ['prefix' => '/api/v1/oauth']);
        $this->app->bind(ConsumerServiceInterface::class, ConsumerService::class);
        $this->app->singleton(ClientInterface::class, function($app) {
            return new Client([
                'headers' => [
                    'user-agent' => env('OUTGOING_USER_AGENT', 'Jitesoft-Http-Proxy')
                ]
            ]);
        });
    }

}
