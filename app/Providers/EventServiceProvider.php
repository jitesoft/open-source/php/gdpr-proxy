<?php

namespace Jitesoft\GdprProxy\Providers;

use Jitesoft\GdprProxy\Events\CompanyRegisteredEvent;
use Jitesoft\GdprProxy\Events\CompanyRemovedEvent;
use Jitesoft\GdprProxy\Events\EndpointRegisteredEvent;
use Jitesoft\GdprProxy\Events\EndpointRemovedEvent;
use Jitesoft\GdprProxy\Events\JobCompletedEvent;
use Jitesoft\GdprProxy\Events\JobStartedEvent;
use Jitesoft\GdprProxy\Events\UserFetchRequestEvent;
use Jitesoft\GdprProxy\Events\UserRemovalRequestedEvent;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        CompanyRegisteredEvent::class => [],
        CompanyRemovedEvent::class => [],
        EndpointRegisteredEvent::class => [],
        EndpointRemovedEvent::class => [],
        JobStartedEvent::class => [],
        JobCompletedEvent::class => [],
        UserRemovalRequestedEvent::class => [],
        UserFetchRequestEvent::class => []
    ];
}
