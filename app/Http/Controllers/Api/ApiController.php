<?php
namespace Jitesoft\GdprProxy\Http\Controllers\Api;

use Jitesoft\GdprProxy\Http\Controllers\Controller;

/**
 * ApiController
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
abstract class ApiController extends Controller {

}
