<?php
namespace Jitesoft\GdprProxy\Http\Controllers\Api;

use Jitesoft\GdprProxy\Events\UserFetchRequestEvent;
use Jitesoft\GdprProxy\Events\UserRemovalRequestedEvent;
use Jitesoft\GdprProxy\Jobs\GetUserDataJob;
use Jitesoft\GdprProxy\Jobs\RemoveUserDataJob;
use Jitesoft\GdprProxy\Models\Company;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * ExternalUserController
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class ExternalUserController extends ApiController {

    private function getConsumer(string $name) : ?Company {
        return Company::where(['name' => $name])->first();
    }

    public function getFetch(Request $request, string $consumer, string $identifier) {
        $consumer = $this->getConsumer($consumer);
        event(new UserFetchRequestEvent($consumer));
        self::dispatch(new GetUserDataJob($consumer, $identifier));
        return new JsonResponse([], 201);
    }

    public function postFetch(Request $request, string $consumer) {
        $consumer = $this->getConsumer($consumer);
        event(new UserFetchRequestEvent($consumer));
        $postData   = $request->post();
        $identifier = array_get($postData, $consumer->identifier_param);
        self::dispatch(new GetUserDataJob($consumer, $identifier));
        return new JsonResponse([], 201);
    }

    public function deleteRemove(Request $request, string $consumer, string $identifier) {
        $consumer = $this->getConsumer($consumer);
        event(new UserRemovalRequestedEvent($consumer));
        self::dispatch(new RemoveUserDataJob($consumer, $identifier));
        return new JsonResponse([], 201);
    }

    public function postRemove(Request $request, string $consumer) {
        $consumer = $this->getConsumer($consumer);
        event(new UserRemovalRequestedEvent($consumer));
        $postData   = $request->post();
        $identifier = array_get($postData, $consumer->identifier_param);
        self::dispatch(new RemoveUserDataJob($consumer, $identifier));
        return new JsonResponse([],201);
    }

}
