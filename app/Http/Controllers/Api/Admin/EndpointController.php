<?php
namespace Jitesoft\GdprProxy\Http\Controllers\Api\Admin;

use function event;
use Exception;
use Jitesoft\GdprProxy\Contracts\ConsumerServiceInterface;
use Jitesoft\GdprProxy\Events\EndpointRegisteredEvent;
use Jitesoft\GdprProxy\Events\EndpointRemovedEvent;
use Jitesoft\GdprProxy\Models\Endpoint;
use function array_key_exists;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use function implode;
use Jitesoft\Exceptions\Http\Client\HttpNotFoundException;

/**
 * EndpointController
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class EndpointController extends AdminController {

    private $consumerService;

    public function __construct(ConsumerServiceInterface $consumerService) {
        $this->consumerService = $consumerService;
    }

    /**
     * @param int $companyId
     * @return JsonResponse
     * @throws HttpNotFoundException
     */
    public function all(int $companyId) {
        return new JsonResponse(
            $this->getCompany($companyId)->endpoints()->get()
        );
    }

    /**
     * @param int $companyId
     * @param int $id
     * @return JsonResponse
     * @throws HttpNotFoundException
     */
    public function get(int $companyId, int $id) {
        try {
            $company = $this->consumerService->getConsumer($companyId);
        } catch (Exception $ex) {
            throw new HttpNotFoundException();
        }

        $endpoint = $company->endpoints()->where(['id' => $id])->first();
        if (!$endpoint) {
            throw new HttpNotFoundException();
        }

        return new JsonResponse([$endpoint]);
    }

    /**
     * @param Request $request
     * @param int $companyId
     * @return JsonResponse
     * @throws HttpNotFoundException
     * @throws ValidationException
     */
    public function create(Request $request, int $companyId) {
        $this->validate($request, [
            'endpoint'  => 'required|url',
            'type'      => 'required|string|in:' . implode(',', Endpoint::ENDPOINT_TYPES),
            'auth_data' => 'required|array',
            'auth_type' => 'required|string'
        ]);

        $company  = $this->getCompany($companyId);
        $endpoint = new Endpoint([
            'endpoint' => $request->post('endpoint'),
            'type'     => $request->post('type')
        ]);

        $endpoint->auth_type = $request->post('auth_type');
        $authData            = $this->consumerService->createAuthenticationData(
            $endpoint->auth_type,
            $request->post('auth_data')
        );

        $endpoint->auth_data = $authData;
        $company->endpoints()->save($endpoint);
        event(new EndpointRegisteredEvent($endpoint));
        return new JsonResponse([$endpoint], 201);
    }

    /**
     * @param Request $request
     * @param int $companyId
     * @param int $id
     * @return JsonResponse
     * @throws HttpNotFoundException
     * @throws ValidationException
     */
    public function update(Request $request, int $companyId, int $id) {
        $this->validate($request, [
            'endpoint'  => 'url',
            'type'      => 'string|in:' . implode(',', Endpoint::ENDPOINT_TYPES),
            'auth_data' => 'array|required_with:auth_type',
            'auth_type' => 'string|required_with:auth_data'
        ]);

        $company  = $this->getCompany($companyId);
        $endpoint = $company->endpoints()->where(['id' => $id])->first();
        if (!$endpoint) {
            throw new HttpNotFoundException();
        }

        $content             = $request->all();
        $endpoint->endpoint  = array_get($content, 'endpoint', $endpoint->endpoint);
        $endpoint->type      = array_get($content, 'type', $endpoint->type);
        $endpoint->auth_type = array_get($content, 'auth_type', $endpoint->auth_type);
        if (array_key_exists('auth_data', $content)) {
            $authData = $this->consumerService->createAuthenticationData(
                $endpoint->auth_type,
                $content['auth_data']
            );

            $endpoint->auth_data = $authData;
        }

        $endpoint->save();
        return new JsonResponse([$endpoint], 202);
    }

    /**
     * @param int $companyId
     * @param int $id
     * @return JsonResponse
     * @throws HttpNotFoundException
     */
    public function remove(int $companyId, int $id) {
        $company  = $this->getCompany($companyId);
        $endpoint = $company->endpoints()->where(['id' => $id])->first();

        if (!$endpoint) {
            throw new HttpNotFoundException();
        }

        $ep = $endpoint->endpoint;
        $endpoint->delete();
        event(new EndpointRemovedEvent($company, $ep, $id));
        return new JsonResponse(['message' => 'Resource deleted.'], 202);
    }

}
