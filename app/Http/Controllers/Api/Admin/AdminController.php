<?php
namespace Jitesoft\GdprProxy\Http\Controllers\Api\Admin;

use Jitesoft\GdprProxy\Http\Controllers\Api\ApiController;
use Jitesoft\GdprProxy\Models\Company;
use Jitesoft\Exceptions\Http\Client\HttpNotFoundException;

/**
 * AdminController
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
abstract class AdminController extends ApiController {

    /**
     * @param int $id
     * @return Company
     * @throws HttpNotFoundException
     */
    protected function getCompany(int $id): Company {
        $company = Company::find($id);
        if (!$company) {
            throw new HttpNotFoundException();
        }

        return $company;
    }

}
