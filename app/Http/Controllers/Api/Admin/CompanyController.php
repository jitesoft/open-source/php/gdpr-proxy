<?php
namespace Jitesoft\GdprProxy\Http\Controllers\Api\Admin;

use function event;
use Jitesoft\GdprProxy\Contracts\ConsumerServiceInterface;
use Jitesoft\GdprProxy\Events\CompanyRegisteredEvent;
use Jitesoft\GdprProxy\Events\CompanyRemovedEvent;
use Jitesoft\GdprProxy\Models\Company;
use function array_has;
use function encrypt;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Jitesoft\Exceptions\Http\Client\HttpNotFoundException;
use function serialize;

/**
 * CompanyController
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class CompanyController extends AdminController {

    private $consumerService;

    public function __construct(ConsumerServiceInterface $consumerService) {
        $this->consumerService = $consumerService;
    }

    public function all() {
        return new JsonResponse(
            Company::all()
        );
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws HttpNotFoundException
     */
    public function get(int $id) {
        $company = Company::find($id);
        if ($company === null) {
            throw new HttpNotFoundException();
        }

        return new JsonResponse([$company], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create(Request $request) {
        $this->validate($request, [
            'name'             => 'string|required',
            'auth_type'        => 'string|required',
            'auth_data'        => 'array|required',
            'identifier_param' => 'string|required'
        ]);

        $company            = new Company(['name' => $request->post('name')]);
        $company->auth_type = $request->post('auth_type');

        $authData = $this->consumerService->createAuthenticationData(
            $company->auth_type,
            $request->post('auth_data')
        );

        $company->auth_data        = $authData;
        $company->identifier_param = $request->post('identifier_param');
        $company->save();
        event(new CompanyRegisteredEvent($company));
        return new JsonResponse([$company], 201);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws HttpNotFoundException
     * @throws ValidationException
     */
    public function update(Request $request, int $id) {
        $this->validate($request, [
            'name'             => 'string',
            'auth_type'        => 'string|required_with:auth_data',
            'auth_data'        => 'array|required_with:auth_type',
            'identifier_param' => 'string'
        ]);

        $company = Company::find($id);
        if ($company === null) {
            throw new HttpNotFoundException();
        }

        $content                   = $request->all();
        $company->name             = array_get($content, 'name', $company->name);
        $company->auth_type        = array_get($content, 'auth_type', $company->auth_type);
        $company->identifier_param = array_get($content, 'identifier_param', $company->identifier_param);
        if (array_has($content, 'auth_data')) {
            $authData = $this->consumerService->createAuthenticationData(
                $company->auth_type,
                $content['auth_data']
            );

            $company->auth_data = $authData;
        }

        $company->save();

        return new JsonResponse([$company], 202);
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws HttpNotFoundException
     */
    public function remove(int $id) {
        $company = Company::find($id);
        if ($company === null) {
            throw new HttpNotFoundException();
        }

        $name = $company->name;
        $company->delete();
        event(new CompanyRemovedEvent($name,$id));
        return new JsonResponse(['message' => 'Resource deleted.'], 202);
    }

}
