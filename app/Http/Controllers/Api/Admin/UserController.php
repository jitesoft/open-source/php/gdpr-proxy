<?php
namespace Jitesoft\GdprProxy\Http\Controllers\Api\Admin;

use Jitesoft\GdprProxy\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Jitesoft\Exceptions\Http\Client\HttpNotFoundException;

/**
 * UserController
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class UserController extends AdminController {

    public function all() {
        return new JsonResponse(User::all());
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws HttpNotFoundException
     */
    public function get(int $id) {
        $user = User::find($id);
        if (!$user) {
            throw new HttpNotFoundException();
        }

        return new JsonResponse([$user]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create(Request $request) {
        $this->validate($request, [
            'name'      => 'string|required',
            'email'    => 'email|required',
            'password' => 'required|string|min:12'
        ]);

        $user = new User([
            'email' => $request->post('email'),
            'name' => $request->post('name'),
            'password' => Hash::make($request->post('password'))
        ]);
        $user->save();
        return new JsonResponse([$user], 201);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws HttpNotFoundException
     * @throws ValidationException
     */
    public function update(Request $request, int $id) {
        $this->validate($request, [
            'name' => 'string',
            'email' => 'email',
            'password' => 'string|min:12'
        ]);

        $user = User::find($id);
        if (!$user) {
            throw new HttpNotFoundException();
        }

        $content     = $request->all();
        $user->name  = array_get($content, 'name', $user->name);
        $user->email = array_get($content, 'email', $user->email);
        if (array_has($content, 'password')) {
            $user->password = Hash::make($content['password']);
        }

        $user->save();
        return new JsonResponse([$user], 202);
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws HttpNotFoundException
     */
    public function remove(int $id) {
        $user = User::find($id);
        if (!$user) {
            throw new HttpNotFoundException();
        }

        $user->delete();
        return new JsonResponse(['message' => 'Resource deleted.'], 202);
    }

}
