<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  CustomerAuth.phphp - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Http\Middleware;

use Jitesoft\Exceptions\Database\Entity\EntityException;
use Jitesoft\Exceptions\Http\Client\HttpNotFoundException;
use Jitesoft\Exceptions\Http\Client\HttpUnauthorizedException;
use Jitesoft\GdprProxy\Contracts\ConsumerServiceInterface;
use Jitesoft\GdprProxy\Models\Company;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Jitesoft\Exceptions\Http\Client\HttpBadRequestException;
use Jitesoft\Exceptions\Security\AuthenticationException;

/**
 * CustomerAuth
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class CustomerAuth {

    private $consumerService;

    public function __construct(ConsumerServiceInterface $consumerService) {
        $this->consumerService = $consumerService;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws HttpBadRequestException
     * @throws HttpNotFoundException
     * @throws HttpUnauthorizedException
     */
    public function handle(Request $request, Closure $next) {
        $consumer = $request->route()[2]['consumer'];
        /** @var Company|null $company */
        try {
            $company = $this->consumerService->getConsumerByName($consumer);
        } catch (EntityException $ex) {
            Log::error($ex->getMessage());
            throw new HttpNotFoundException();
        }

        try {
            $authenticator = $this->consumerService->resolveAuthenticationType($company->auth_type, $company);
        } catch (AuthenticationException $ex) {
            Log::error($ex->message);
            throw new HttpBadRequestException('Invalid authentication type.');
        }

        try {
            if ($authenticator->evaluate($request)) {
                return $next($request);
            }
        } catch (AuthenticationException $ex) {
            Log::error($ex->message);
        }

        throw new HttpUnauthorizedException();
    }

}
