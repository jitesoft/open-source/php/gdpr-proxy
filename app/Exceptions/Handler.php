<?php
namespace Jitesoft\GdprProxy\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e) {
        if ($e instanceof \Jitesoft\Exceptions\Http\HttpException) {
            return new JsonResponse(['message' => $e->getMessage()], $e->getCode());
        } else if ($e instanceof ValidationException) {
            return new JsonResponse($e->errors(), 400);
        } else if ($e instanceof NotFoundHttpException) {
            return new JsonResponse(['message' => 'Resource not found.'], 404);
        }

        return parent::render($request, $e);
    }

}
