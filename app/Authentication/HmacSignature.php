<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  HmacSignature.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Authentication;

use Jitesoft\Exceptions\Security\AuthenticationException;
use Jitesoft\GdprProxy\Contracts\AuthenticationMethodInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * HmacSignature
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class HmacSignature implements AuthenticationMethodInterface {
    public const TYPE = 'HmacSignature';

    private $privateKey;
    private $header;
    private $algorithm;

    /**
     * HmacSignature constructor.
     * @param array $authenticationData
     * @throws AuthenticationException
     */
    public function __construct(array $authenticationData) {
        if (!array_key_exists('signature', $authenticationData) ||
            !array_key_exists('header_name', $authenticationData) ||
            !array_key_exists('algorithm', $authenticationData)) {
            throw new AuthenticationException('Property mismatch.');
        }

        $this->privateKey = $authenticationData['signature'];
        $this->header     = $authenticationData['header_name'];
        $this->algorithm  = $authenticationData['algorithm'];
    }

    /**
     * @param Request $request
     * @return bool
     * @throws AuthenticationException
     */
    public function evaluate(Request $request): bool {
        if (!$request->hasHeader($this->header)) {
            throw new AuthenticationException('Invalid headers.');
        }

        $signature         = $request->header($this->header);
        $computedSignature = $this->encode($request->getContent());
        return hash_equals($signature, $computedSignature);
    }

    private function encode(string $data): string {
        $hash = hash_hmac($this->algorithm, $data, $this->privateKey, true);
        return base64_encode($hash);
    }

    /**
     * Get the headers used when sending request.
     *
     * @param $payload
     * @return array
     */
    public function getHeaders($payload): array {
        return [
            'X-Authorization-Timestamp' => Carbon::now()->timestamp,
            'Authentication'            => sprintf('signature %s', $this->encode($payload))
        ];
    }

}
