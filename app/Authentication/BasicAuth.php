<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  BasicAuth.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Authentication;

use function array_key_exists;
use Jitesoft\GdprProxy\Contracts\AuthenticationMethodInterface;
use Illuminate\Http\Request;
use Jitesoft\Exceptions\Security\AuthenticationException;

/**
 * BasicAuth
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class BasicAuth implements AuthenticationMethodInterface {
    /** @var string */
    public const TYPE = 'BasicAuth';
    /** @var string  */
    private $userName;
    /** @var string  */
    private $password;

    /**
     * BasicAuth constructor.
     * @param array $authenticationData
     * @throws AuthenticationException
     */
    public function __construct(array $authenticationData) {
        if (!array_key_exists('username', $authenticationData) || !array_key_exists('password', $authenticationData)) {
            throw new AuthenticationException('Property mismatch.');
        }

        $this->userName = $authenticationData['username'];
        $this->password = $authenticationData['password'];
    }

    public function evaluate(Request $request): bool {
        $data  = $request->header('Authorization');
        $split = explode(' ', $data);
        if ($split[0] !== 'Basic') {
            throw new AuthenticationException('Invalid authentication type.');
        }

        $info = explode(':', base64_decode($split[1]));
        return $info[0] === $this->userName && $info[1] === $this->password;
    }

    public function getHeaders($payload): array {

        $ba = sprintf(
            'Basic %s',
            base64_encode(sprintf(
                '%s:%s',
                $this->userName,
                $this->password
            ))
        );

        return [
            'Authorization' => $ba
        ];
    }

}
