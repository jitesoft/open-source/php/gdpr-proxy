<?php
namespace Jitesoft\GdprProxy\Jobs;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Log;
use Jitesoft\GdprProxy\Contracts\HttpServiceInterface;
use Jitesoft\GdprProxy\Events\JobCompletedEvent;
use Jitesoft\GdprProxy\Events\JobStartedEvent;
use Jitesoft\GdprProxy\Models\Company;
use Jitesoft\GdprProxy\Models\Endpoint;
use Jitesoft\GdprProxy\Models\JobLog;
use function json_encode;
use function var_dump;

/**
 * GetUserDataJob
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class GetUserDataJob extends Job {

    protected $user;
    protected $company;
    protected $callback;

    public function __construct(Company $company, string $userIdentifier, ?string $callback = null) {
        $this->callback = $callback;
        $this->user     = $userIdentifier;
        $this->company  = $company;
    }

    public function handle() {
        // TODO: This method have to be able to parse the response from the
        // endpoints and send it in the callback as a common format.
        event(new JobStartedEvent($this->company, Endpoint::FETCH));
        /** @var ClientInterface $client */
        $client    = app(ClientInterface::class);
        $endpoints = $this->company->endpoints()->where(['type' => Endpoint::FETCH])->get();
        $company   = $this->company;
        $pool      = new Pool($client, $this->setUpRequests($endpoints), [
            'concurrency' => 5,
            'fulfilled' => function(Response $response, $index) use($company, $endpoints) {
                $log = new JobLog([
                    'result' => 'success',
                    'reason' => $response->getReasonPhrase(),
                    'job_data' => [
                        'status_code' => $response->getStatusCode(),
                        'protocol'    => $response->getProtocolVersion(),
                        'company'     => $company->id,
                        'endpoint'    => [
                            'id'       => $endpoints[$index]->id,
                            'endpoint' => $endpoints[$index]->endpoint
                        ]
                    ]
                ]);
                $log->save();
                Log::debug('Request fulfilled');
            },
            'rejected' => function($reason, $index) use($company, $endpoints) {
                Log::debug('Request rejected');
                $log = new JobLog([
                    'result' => 'failure',
                    'reason' => $reason->getMessage(),
                    'job_data' => [
                        'status_code' => null,
                        'protocol'    => null,
                        'company'     => $company->id,
                        'endpoint'    => [
                            'id'       => $endpoints[$index]->id,
                            'endpoint' => $endpoints[$index]->endpoint
                        ]
                    ]
                ]);
                $log->save();
            }
        ]);

        $promise = $pool->promise();
        $result  = $promise->wait();
        if ($this->callback !== null) {
            $request = new Request('POST', $this->callback, [], json_encode([
                'result' => $result
            ]));
            $client->send($request);
        }

        event(new JobCompletedEvent($this->company, Endpoint::FETCH, false));
    }

    /**
     * @param Endpoint[] $endpoints
     * @return \Generator
     */
    private function setUpRequests($endpoints) {
        $result = [];
        foreach ($endpoints as $endpoint) {
            $url = sprintf('%s/%s', $endpoint->endpoint, $this->user);
            yield new Request('GET', $url, $endpoint->auth_data->getHeaders(''));
        }
    }

}
