<?php
namespace Jitesoft\GdprProxy\Jobs;

use function event;
use GuzzleHttp\ClientInterface;
use Jitesoft\GdprProxy\Contracts\HttpServiceInterface;
use Jitesoft\GdprProxy\Events\JobCompletedEvent;
use Jitesoft\GdprProxy\Events\JobStartedEvent;
use Jitesoft\GdprProxy\Models\Company;
use Jitesoft\GdprProxy\Models\Endpoint;
use Jitesoft\GdprProxy\Models\JobLog;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Log;

/**
 * RemoveUserDataJob
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class RemoveUserDataJob extends Job {

    protected $user;
    protected $company;

    public function __construct(Company $company, string $userIdentifier) {
        $this->user    = $userIdentifier;
        $this->company = $company;
    }

    public function handle() {
        event(new JobStartedEvent($this->company, Endpoint::REMOVE));

        $company   = $this->company;
        $client    = app(ClientInterface::class);
        $endpoints = $this->company->endpoints()->where(['type' => Endpoint::REMOVE])->get();
        $pool      = new Pool($client, $this->setUpRequests($endpoints), [
            'concurrency' => 5,
            'fulfilled' => function(Response $response, $index) use($company, $endpoints) {
                $log = new JobLog([
                    'result' => 'success',
                    'reason' => $response->getReasonPhrase(),
                    'job_data' => [
                        'status_code' => $response->getStatusCode(),
                        'protocol'    => $response->getProtocolVersion(),
                        'company'     => $company->id,
                        'endpoint'    => [
                            'id'       => $endpoints[$index]->id,
                            'endpoint' => $endpoints[$index]->endpoint
                        ]
                    ]
                ]);
                $log->save();
                Log::debug('Request fulfilled');
            },
            'rejected' => function($reason, $index) use($company, $endpoints) {
                Log::debug('Request rejected');
                $log = new JobLog([
                    'result' => 'failure',
                    'reason' => $reason->getMessage(),
                    'job_data' => [
                        'status_code' => null,
                        'protocol'    => null,
                        'company'     => $company->id,
                        'endpoint'    => [
                            'id'       => $endpoints[$index]->id,
                            'endpoint' => $endpoints[$index]->endpoint
                        ]
                    ]
                ]);
                $log->save();
            }
        ]);

        $promise = $pool->promise();
        $promise->wait();

        event(new JobCompletedEvent($this->company, Endpoint::REMOVE, true));
    }

    private function setUpRequests($endpoints) {
        $urlFormat = '%s/%s';
        foreach ($endpoints as $endpoint) {
            $url = sprintf($urlFormat, $endpoint->endpoint, urlencode($this->user));
            yield new Request('DELETE', $url, $endpoint->auth_data->getHeaders(''));
        }
    }

}
