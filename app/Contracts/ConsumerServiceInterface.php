<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  ConsumerServiceInterface.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Contracts;

use Jitesoft\Exceptions\Database\Entity\EntityException;
use Jitesoft\GdprProxy\Models\Company;
use Jitesoft\Exceptions\Security\AuthenticationException;

/**
 * ConsumerServiceInterface
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
interface ConsumerServiceInterface {

    /**
     * Get a company based on id.
     *
     * @param int $id
     * @return Company
     * @throws EntityException
     */
    public function getConsumer(int $id): Company;

    /**
     * @param string $name
     * @return Company
     * @throws EntityException
     */
    public function getConsumerByName(string $name): Company;

    /**
     * Get the authentication method of a given company.
     *
     * @param string $authType
     * @param Company $company
     * @return AuthenticationMethodInterface|null
     * @throws AuthenticationException
     */
    public function resolveAuthenticationType(string $authType, Company $company): ?AuthenticationMethodInterface;

    /**
     * Create encrypted authentication data.
     *
     * @param string $type
     * @param array $data
     * @return AuthenticationMethodInterface
     */
    public function createAuthenticationData(string $type, array $data): AuthenticationMethodInterface;

}
