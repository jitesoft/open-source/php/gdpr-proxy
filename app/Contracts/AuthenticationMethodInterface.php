<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  AuthenticationMethodInterface.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Contracts;

use Illuminate\Http\Request;
use Jitesoft\Exceptions\Security\AuthenticationException;

/**
 * AuthenticationMethodInterface
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
interface AuthenticationMethodInterface {

    public function __construct(array $authenticationData);

    /**
     * Get the headers used when sending request.
     *
     * @param $payload
     * @return array
     */
    public function getHeaders($payload): array;

    /**
     * Compare given data to the authentication methods credentials.
     *
     * @param Request $request
     * @return bool
     * @throws AuthenticationException
     */
    public function evaluate(Request $request): bool;

}
