<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  ConsumerService.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Services;

use Jitesoft\Exceptions\Database\Entity\EntityException;
use Jitesoft\GdprProxy\Authentication\BasicAuth;
use Jitesoft\GdprProxy\Authentication\HmacSignature;
use Jitesoft\GdprProxy\Contracts\AuthenticationMethodInterface;
use Jitesoft\GdprProxy\Contracts\ConsumerServiceInterface;
use Jitesoft\GdprProxy\Models\Company;
use Illuminate\Support\Facades\Log;
use Jitesoft\Exceptions\Security\AuthenticationException;

/**
 * ConsumerService
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class ConsumerService implements ConsumerServiceInterface {

    private $authMethods = [
        BasicAuth::TYPE     => BasicAuth::class,
        HmacSignature::TYPE => HmacSignature::class
    ];

    public function getConsumer(int $id): Company {
        $consumer = Company::find($id);
        if (!$consumer) {
            throw new EntityException('Entity not found.');
        }

        return $consumer;
    }

    public function resolveAuthenticationType(string $authType, Company $company): ?AuthenticationMethodInterface {
        if (!array_key_exists($authType, $this->authMethods)) {
            Log::error('AuthType "{type}" invalid', [
                "type" => $authType
            ]);
            throw new AuthenticationException('Invalid authentication type.');
        }

        $out = $company->auth_data;

        if (!$out) {
            throw new AuthenticationException('Failed to fetch authentication data.');
        }

        return $out;
    }

    /**
     * Create encrypted authentication data.
     *
     * @param string $type
     * @param array $data
     * @return AuthenticationMethodInterface
     * @throws AuthenticationException
     */
    public function createAuthenticationData(string $type, array $data): AuthenticationMethodInterface {
        if (!array_key_exists($type, $this->authMethods)) {
            throw new AuthenticationException('Invalid authentication type.');
        }

        $class = $this->authMethods[$type];
        return new $class($data);
    }

    /**
     * @param string $name
     * @return Company
     * @throws EntityException
     */
    public function getConsumerByName(string $name): Company {
        $consumer = Company::where(['name' => $name])->first();
        if (!$consumer) {
            throw new EntityException('Failed to fetch entity.');
        }

        return $consumer;
    }

}
