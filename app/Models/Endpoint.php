<?php
namespace Jitesoft\GdprProxy\Models;

use Jitesoft\GdprProxy\Contracts\AuthenticationMethodInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Endpoint
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 *
 * @property string $endpoint
 * @property string $type
 * @property string $auth_type
 * @property AuthenticationMethodInterface $auth_data
 */
class Endpoint extends Model {
    public const FETCH  = 'fetch';
    public const REMOVE = 'delete';

    public const ENDPOINT_TYPES = [
        self::FETCH,
        self::REMOVE
    ];

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function getAuthDataAttribute(): AuthenticationMethodInterface {
        return unserialize(decrypt($this->attributes['auth_data']));
    }

    public function setAuthDataAttribute($value) {
        $this->attributes['auth_data'] = encrypt(serialize($value));
    }

    protected $hidden   = ['auth_type', 'auth_data'];
    protected $fillable = ['endpoint', 'type', 'auth_type', 'auth_data'];

}
