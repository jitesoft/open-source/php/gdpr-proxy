<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  JobLog.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace Jitesoft\GdprProxy\Models;

use function encrypt;
use Illuminate\Database\Eloquent\Model;

/**
 * JobLog
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class JobLog extends Model {

    public function setJobDataAttribute($value) {
        $this->attributes['job_data'] = encrypt(serialize($value));
    }

    public function setReasonAttribute($value) {
        $this->attributes['reason'] = encrypt($value);
    }

    protected $hidden = ['job_data'];

    protected $fillable = ['job_data', 'result', 'reason'];
}
