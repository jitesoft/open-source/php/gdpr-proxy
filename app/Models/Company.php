<?php
namespace Jitesoft\GdprProxy\Models;

use Illuminate\Database\Eloquent\Collection;
use Jitesoft\GdprProxy\Contracts\AuthenticationMethodInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Company
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 *
 * @property Endpoint[]|Collection $endpoints
 * @property string $name
 * @property string $auth_type
 * @property AuthenticationMethodInterface $auth_data
 * @property string $identifier_param
 */
class Company extends Model {

    public function endpoints() {
        return $this->hasMany(Endpoint::class);
    }

    public function getAuthDataAttribute(): AuthenticationMethodInterface {
        return unserialize(decrypt($this->attributes['auth_data']));
    }

    public function setAuthDataAttribute($value) {
        $this->attributes['auth_data'] = encrypt(serialize($value));
    }

    protected $hidden   = ['auth_type', 'auth_data'];
    protected $fillable = ['name', 'auth_type', 'auth_data', 'identifier_param'];
}
