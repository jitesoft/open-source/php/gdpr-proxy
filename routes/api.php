<?php

/** @var $router \Laravel\Lumen\Routing\Router */

use Jitesoft\GdprProxy\Http\Controllers\Api\Admin\CompanyController;
use Jitesoft\GdprProxy\Http\Controllers\Api\Admin\EndpointController;
use Jitesoft\GdprProxy\Http\Controllers\Api\Admin\UserController;
use Jitesoft\GdprProxy\Http\Controllers\Api\ExternalUserController;

$router->group([ 'prefix' => 'v1' ], function() use ($router) {

    $router->get('/', function () {
        return [ 'version' => '1.0.0' ];
    });

    $router->group(['prefix' => '/admin', 'middleware' => 'auth:api'], function() use ($router) {

        $router->get('/companies', CompanyController::class . '@all');
        $router->get('/company/{id}', CompanyController::class . '@get');
        $router->post('/company', CompanyController::class . '@create');
        $router->put('/company/{id}', CompanyController::class . '@update');
        $router->delete('/company/{id}', CompanyController::class . '@remove');

        $router->get('/users',  UserController::class . '@all');
        $router->get('/user/{id}', UserController::class . '@get');
        $router->post('/user', UserController::class . '@create');
        $router->put('/user/{id}', UserController::class . '@update');
        $router->delete('/user/{id}', UserController::class . '@remove');

        $router->get('/company/{id}/endpoints', EndpointController::class . '@all');
        $router->get('/company/{id}/endpoint/{identifier}', EndpointController::class . '@get');
        $router->post('/company/{id}/endpoint', EndpointController::class . '@create');
        $router->put('/company/{id}/endpoint/{identifier}', EndpointController::class . '@update');
        $router->delete('/company/{id}/endpoint/{identifier}', EndpointController::class . '@remove');
    });

    $router->group(['middleware' => 'consumer'], function () use ($router) {
        $router->get('/{consumer}/user/{identifier}', ExternalUserController::class . '@getFetch');
        $router->post('/{consumer}/user/fetch', ExternalUserController::class . '@postFetch');
        $router->delete('/{consumer}/user/{identifier}', ExternalUserController::class . '@deleteRemove');
        $router->post('/{consumer}/user/remove', ExternalUserController::class . '@postRemove');
    });
});
