<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  BasicAuthTest.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace App\Tests\Authentication;

use function base64_encode;
use Illuminate\Http\Request;
use Jitesoft\Exceptions\Security\AuthenticationException;
use Jitesoft\GdprProxy\Authentication\BasicAuth;
use Jitesoft\GdprProxy\Contracts\AuthenticationMethodInterface;
use TestCase;

/**
 * BasicAuthTest
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class BasicAuthTest extends TestCase {

    public function setUp() {
        parent::setUp(); // TODO: Change the autogenerated stub
    }

    public function testConstructorWithValidData() {
        $auth = new BasicAuth([
            'username' => 'abc',
            'password' => 'efg'
        ]);

        $this->assertInstanceOf(AuthenticationMethodInterface::class, $auth);
    }

    public function testConstructorWithInvalidData() {
        $this->expectException(AuthenticationException::class);
        $auth = new BasicAuth(['a' => 'b']);
    }

    public function testGetHeaders() {
        $auth = new BasicAuth(['username' => 'abc', 'password' => 'efg']);
        $shouldBe = [
            'Authorization' => 'Basic ' . base64_encode('abc:efg')
        ];

        $this->assertEquals($shouldBe, $auth->getHeaders(''));
    }

    public function testEvaluateWithValidData() {
        $auth = new BasicAuth(['username' => 'abc', 'password' => 'efg']);

        $request = new Request();
        $request->headers->add(['Authorization' => 'Basic ' . base64_encode('abc:efg')]);
        $result = $auth->evaluate($request);
        $this->assertTrue($result);
    }

    public function testEvaluateWithInvalidData() {
        $auth = new BasicAuth(['username' => 'abc', 'password' => 'efg']);

        $this->expectException(AuthenticationException::class);

        $request = new Request();
        $request->headers->add(['Authorization' => 'Basicz ' . base64_encode('abc:efg')]);
        $auth->evaluate($request);
    }

    public function testEvaluateWithValidMethodInvalidCredentials() {
        $auth = new BasicAuth(['username' => 'abc', 'password' => 'efg']);

        $request = new Request();
        $request->headers->add(['Authorization' => 'Basic ' . base64_encode('abc:efgh')]);
        $result = $auth->evaluate($request);

        $this->assertFalse($result);
    }

}
