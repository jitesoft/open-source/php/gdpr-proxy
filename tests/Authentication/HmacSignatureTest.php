<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  HmacSignatureTest.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace App\Tests\Authentication;

use Illuminate\Http\Request;
use Jitesoft\Exceptions\Security\AuthenticationException;
use Jitesoft\GdprProxy\Authentication\HmacSignature;
use Jitesoft\GdprProxy\Contracts\AuthenticationMethodInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use TestCase;

/**
 * HmacSignatureTest
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class HmacSignatureTest extends TestCase {

    private $privateKey = 'bji6VYu9Ejj8PnD6Sm9YdVthqod2laZr';

    public function testConstructorWithValidData() {
        $sign = new HmacSignature([
            'signature' => $this->privateKey,
            'header_name' => 'sign',
            'algorithm' => 'sha256'
        ]);

        $this->assertInstanceOf(AuthenticationMethodInterface::class, $sign);
    }

    public function testConstructorWithInvalidData() {
        $this->expectException(AuthenticationException::class);

        new HmacSignature([
            'signature' => $this->privateKey,
            'header_name' => 'sign',
        ]);
    }

    public function testEvaluateWithValidData() {
        $auth = new HmacSignature([
            'signature' => $this->privateKey,
            'header_name' => 'sign',
            'algorithm' => 'sha256'
        ]);

        $request = new Request();
        $request->setJson(new ParameterBag([
            'abc' => 'str',
            'efg' => 5
        ]));

        // Make sign.
        $sign = base64_encode(hash_hmac('sha256', $request->getContent(), $this->privateKey, true));
        $request->headers->add([
            'sign' => $sign
        ]);

        $result = $auth->evaluate($request);
        $this->assertTrue($result);
    }

    public function testEvaluateWithInvalidData() {
        $auth = new HmacSignature([
            'signature' => $this->privateKey,
            'header_name' => 'signature',
            'algorithm' => 'sha256'
        ]);

        $request = new Request();
        $request->setJson(new ParameterBag([
            'abc' => 'str',
            'efg' => 5
        ]));

        // Make sign.
        $sign = hash_hmac('sha256', $request->getContent(), $this->privateKey, true);
        $request->headers->add([
            'sign' => $sign
        ]);

        $this->expectException(AuthenticationException::class);
        $auth->evaluate($request);
    }

    public function testEvaluateWithInvalidCredentials() {
        $auth = new HmacSignature([
            'signature' => $this->privateKey,
            'header_name' => 'sign',
            'algorithm' => 'sha256'
        ]);

        $request = new Request();
        $request->setJson(new ParameterBag([
            'abc' => 'str',
            'efg' => 5
        ]));

        // Make sign.
        $sign = hash_hmac('sha256', $request->getContent(), 'abc123', true);
        $request->headers->add([
            'sign' => $sign
        ]);

        $result = $auth->evaluate($request);
        $this->assertFalse($result);
    }

}
