<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  RemoveUserDataJobTest.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace App\Tests\Jobs;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Promise\RejectedPromise;
use GuzzleHttp\Psr7\Response;
use Jitesoft\GdprProxy\Events\JobCompletedEvent;
use Jitesoft\GdprProxy\Events\JobStartedEvent;
use Jitesoft\GdprProxy\Jobs\RemoveUserDataJob;
use Jitesoft\GdprProxy\Models\Company;
use TestCase;

/**
 * RemoveUserDataJobTest
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class RemoveUserDataJobTest extends TestCase {

    public function testHandleSuccess() {
        $clientMock = $this->getMockBuilder(ClientInterface::class)->getMock();
        $clientMock->method('sendAsync')->willReturn(new FulfilledPromise(new Response(
            200, [], null, 1.1, 'Reason!'
        )));

        $this->app->singleton(ClientInterface::class, function($a) use($clientMock) {
            return $clientMock;
        });

        $company = new Company();
        $company->endpoints->add(['endpoint' => 'https://localhost', 'type' => 'delete']);
        $company->endpoints->add(['endpoint' => 'https://localhost', 'type' => 'fetch']);

        $job = new RemoveUserDataJob($company, 'abc@efg.com');
        $this->expectsEvents([JobStartedEvent::class, JobCompletedEvent::class]);
        $job->handle();
    }

    public function testHandleError() {
        $clientMock = $this->getMockBuilder(ClientInterface::class)->getMock();
        $clientMock->method('sendAsync')->willReturn(new RejectedPromise('Cause I said so!'));

        $this->app->singleton(ClientInterface::class, function($a) use($clientMock) {
            return $clientMock;
        });

        $company = new Company();
        $company->endpoints->add(['endpoint' => 'https://localhost', 'type' => 'delete']);
        $company->endpoints->add(['endpoint' => 'https://localhost', 'type' => 'fetch']);

        $job = new RemoveUserDataJob($company, 'abc@efg.com');
        $this->expectsEvents([JobStartedEvent::class, JobCompletedEvent::class]);
        $job->handle();
    }

}
