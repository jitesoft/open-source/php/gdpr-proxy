<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  CustomerAuthTest.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace App\Tests\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Jitesoft\Exceptions\Http\Client\HttpBadRequestException;
use Jitesoft\GdprProxy\Authentication\BasicAuth;
use Jitesoft\GdprProxy\Contracts\ConsumerServiceInterface;
use Jitesoft\GdprProxy\Http\Middleware\CustomerAuth;
use Jitesoft\GdprProxy\Models\Company;
use Laravel\Lumen\Routing\Router;
use TestCase;

/**
 * CustomerAuthTest
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class CustomerAuthTest extends TestCase {


    public function testHandleWithInvalidConsumer() {
        $this->get('/api/v1/test/user/a@b.c', [
            'Authorization' => 'Basic ' . base64_encode('abc:efg')
        ]);

        $this->assertResponseStatus(404);
    }

    public function testHandleWithInvalidCredentials() {
        $consumer = new Company(['name' => 'test', 'auth_data' => new BasicAuth([
            'username' => 'abc123',
            'password' => '321cba'
        ]), 'auth_type' => 'BasicAuth']);
        $consumer->save();

        $this->get('/api/v1/test/user/a@b.c', [
            'Authorization' => 'Basic ' . base64_encode('abc:efg')
        ]);

        $this->assertResponseStatus(401);
    }


    public function testHandleSuccess() {
        $consumer = new Company(['name' => 'test', 'auth_data' => new BasicAuth([
            'username' => 'abc123',
            'password' => '321cba'
        ]), 'auth_type' => 'BasicAuth']);
        $consumer->save();

        $router = app()->router;

        $called = false;
        $router->group(['middleware' => CustomerAuth::class], function($router) use(&$called) {
            $router->get('testing/{consumer}', function() use(&$called) {
                $called = true;
                return new Response('{}', 200);
            });
        });

        $this->get('/testing/test', [
            'Authorization' => 'Basic ' . base64_encode('abc123:321cba')
        ]);

        $this->assertResponseOk();
        $this->assertTrue($called);
    }


}
