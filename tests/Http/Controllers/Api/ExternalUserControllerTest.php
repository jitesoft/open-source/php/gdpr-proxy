<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  ExternalUserControllerTest.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace App\Tests\Http\Controllers\Api;

use EventsListener;
use Jitesoft\GdprProxy\Events\UserFetchRequestEvent;
use Jitesoft\GdprProxy\Events\UserRemovalRequestedEvent;
use Jitesoft\GdprProxy\Jobs\GetUserDataJob;
use Jitesoft\GdprProxy\Jobs\RemoveUserDataJob;
use Jitesoft\GdprProxy\Models\Company;
use TestCase;

/**
 * ExternalUserControllerTest
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class ExternalUserControllerTest extends TestCase {

    public function setUp() {
        parent::setUp();
        $this->withoutMiddleware();
        $this->withoutJobs();
    }

    private function setUpCompany() {
        $company = new Company([
            'name' => 'company',
            'identifier_param' => 'payload.identifier'
        ]);
        $company->save();
        return $company;
    }

    public function testGetFetch() {
        $this->expectsEvents(UserFetchRequestEvent::class);
        $this->expectsJobs(GetUserDataJob::class);

        $company = $this->setUpCompany();

        $this->get(sprintf('/api/v1/%s/user/%s', $company->name, 'abc@efg.com'));
        $this->assertResponseStatus(201);
    }

    public function testPostFetch() {
        $this->expectsEvents(UserFetchRequestEvent::class);
        $this->expectsJobs(GetUserDataJob::class);

        $company = $this->setUpCompany();

        $this->post(sprintf('/api/v1/%s/user/fetch', $company->name), ['payload' => [ 'identifier' => 'abc@efg.com'] ]);
        $this->assertResponseStatus(201);
    }

    public function testDeleteRemove() {
        $this->expectsEvents(UserRemovalRequestedEvent::class);
        $this->expectsJobs(RemoveUserDataJob::class);

        $company = $this->setUpCompany();

        $this->delete(sprintf('/api/v1/%s/user/%s', $company->name, 'abc@efg.com'));
        $this->assertResponseStatus(201);
    }

    public function testPostRemove() {
        $this->expectsEvents(UserRemovalRequestedEvent::class);
        $this->expectsJobs(RemoveUserDataJob::class);

        $company = $this->setUpCompany();

        $this->post(sprintf('/api/v1/%s/user/remove', $company->name), ['payload' => [ 'identifier' => 'abc@efg.com'] ]);
        $this->assertResponseStatus(201);
    }

}
