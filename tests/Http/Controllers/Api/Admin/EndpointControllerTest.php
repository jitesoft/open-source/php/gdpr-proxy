<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  EndpointControllerTest.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace App\Tests\Http\Controllers\Api\Admin;
use Jitesoft\GdprProxy\Models\Company;
use Jitesoft\GdprProxy\Models\Endpoint;
use TestCase;

/**
 * EndpointControllerTest
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class EndpointControllerTest extends TestCase {

    public function setUp() {
        parent::setUp();

        $this->withoutMiddleware();
    }

    private function setUpData() {

        $companies = [];
        for ($i=0;$i<3;$i++) {
            $company = new Company(['name' => 'company' . $i]);
            $company->save();
            for ($j=0;$j<3;$j++) {
                $company->endpoints()->save(new Endpoint([
                    'endpoint' => 'localhost-' . $i . ':' . $j,
                ]));
            }
            $companies[] = $company;
        }

        return $companies;
    }

    public function testGet() {
        $company  = $this->setUpData()[0];
        $endpoint = $company->endpoints[0];

        $uri = '/api/v1/admin/company/' . $company->id . '/endpoint/' . $endpoint->id;
        $this->get($uri)->seeJson([
            "endpoint" => 'localhost-0:0'
        ]);
        $this->assertResponseOk();
    }

    public function testGetNotFound() {
        $this->get('/api/v1/admin/company/1/endpoint/1');
        $this->assertResponseStatus(404);
    }

    public function testAll() {
        $companies = $this->setUpData();

        $this->get('/api/v1/admin/company/' . $companies[0]->id . '/endpoints');
        $json = json_decode($this->response->getContent(), true);
        $this->assertArraySubset([
            [
                'company_id' => $companies[0]->id,
                'id' => $companies[0]->endpoints[0]->id,
                'endpoint' => 'localhost-0:0'
            ],
            [
                'company_id' => $companies[0]->id,
                'id' => $companies[0]->endpoints[1]->id,
                'endpoint' => 'localhost-0:1'
            ],
            [
                'company_id' => $companies[0]->id,
                'id' => $companies[0]->endpoints[2]->id,
                'endpoint' => 'localhost-0:2'
            ]
        ], $json);

        $this->assertResponseOk();
    }

    public function testCreate() {
        $companies = $this->setUpData();

        $this->post('/api/v1/admin/company/' . $companies[0]->id . '/endpoint', [
            'endpoint' => 'https://localhost/abc',
            'type' => 'fetch',
            'auth_type' => 'BasicAuth',
            'auth_data' => [
                'username' => 'abc',
                'password' => 'efg'
            ]
        ])->seeJson([
            'type' => 'fetch',
            'endpoint' => 'https://localhost/abc',
            'company_id' => $companies[0]->id
        ]);
        $this->assertResponseStatus(201);
        $this->seeInDatabase('endpoints', [
            'company_id' => $companies[0]->id,
            'endpoint' => 'https://localhost/abc'
        ]);
    }

    public function testCreateInvalidData() {
        $companies = $this->setUpData();

        $this->post('/api/v1/admin/company/' . $companies[0]->id . '/endpoint', [
            'endpoint' => 'https://localhost/abc',
        ]);
        $this->assertResponseStatus(400);
    }

    public function testUpdate() {
        $companies = $this->setUpData();
        $endpoint  = $companies[0]->endpoints[0];
        $old       = $endpoint->endpoint;
        $this->put('/api/v1/admin/company/' . $companies[0]->id . '/endpoint/' . $endpoint->id, [
            'endpoint' => 'https://example.com'
        ])->seeJson([
            'id' => $endpoint->id,
            'endpoint' => 'https://example.com'
        ]);

        $this->assertResponseStatus(202);
        $this->notSeeInDatabase('endpoints', [
            'endpoint' => $old
        ]);
        $this->seeInDatabase('endpoints', [
            'endpoint' => 'https://example.com'
        ]);
    }

    public function testUpdateInvalidData() {
        $companies = $this->setUpData();
        $this->put('/api/v1/admin/company/' . $companies[0]->id . '/endpoint/' . $companies[0]->endpoints[0]->id, [
            'a' => 'b'
        ]);
        $this->assertResponseStatus(202); // TODO: Should return a response with no change.
    }

    public function testRemove() {
        $companies = $this->setUpData();
        $removeId = $companies[0]->endpoints[0]->id;

        $this->delete('/api/v1/admin/company/' . $companies[0]->id . '/endpoint/' . $removeId);
        $this->notSeeInDatabase('endpoints',
            [
                'id' => $removeId
            ]
        );
        $this->assertResponseStatus(202);
    }

    public function testRemoveNotFound() {
        $this->delete('/api/v1/admin/endpoint/1');
        $this->assertResponseStatus(404);
    }
}
