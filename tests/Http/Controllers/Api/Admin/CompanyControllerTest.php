<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  CompanyControllerTest.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace App\Tests\Http\Controllers\Api\Admin;

use Jitesoft\GdprProxy\Models\Company;
use function json_decode;
use TestCase;

/**
 * CompanyControllerTest
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class CompanyControllerTest extends TestCase {

    public function setUp() {
        parent::setUp();

        $this->withoutMiddleware();
    }

    private function setUpData() {
        $companies = [];
        for ($i=0;$i<3;$i++) {
            $companies[] = new Company(['name' => 'abc'.$i]);
            $companies[$i]->save();
        }
        return $companies;
    }

    public function testGet() {
        $c = $this->setUpData();
        $this->get('api/v1/admin/company/' . $c[0]->id)->seeJson([
            'id' => $c[0]->id,
            'name' => $c[0]->name
        ]);
        $this->assertResponseOk();
    }

    public function testGetNotFound() {
        $this->setUpData();
        $this->get('api/v1/admin/company/500');
        $this->assertResponseStatus(404);
    }

    public function testAll() {
        $c = $this->setUpData();
        $this->get('api/v1/admin/companies');
        $json = json_decode($this->response->getContent(), true);
        $this->assertArraySubset([
            [
                'id' => $c[0]->id,
                'name' => $c[0]->name
            ],
            [
                'id' => $c[1]->id,
                'name' => $c[1]->name
            ],
            [
                'id' => $c[2]->id,
                'name' => $c[2]->name
            ]
        ], $json);
        $this->assertResponseOk();
    }

    public function testCreate() {
        $this->setUpData();

        $this->post('/api/v1/admin/company', [
            'name' => 'abc123',
            'auth_type' => 'BasicAuth',
            'auth_data' => [
                'username' => 'abc',
                'password' => 'efg'
            ],
            'identifier_param' => 'a.b.c'
        ])->seeJson([
            'name' => 'abc123',
            'identifier_param' => 'a.b.c'
        ]);

        $this->seeInDatabase('companies', [
            'name' => 'abc123'
        ]);

        $this->assertResponseStatus(201);
    }

    public function testCreateInvalidData() {
        $this->post('/api/v1/admin/company', [
            'a' => 'b'
        ]);
        $this->assertResponseStatus(400);
    }

    public function testUpdate() {
        $c = $this->setUpData();
        $toUpdate = $c[0];

        $this->put('/api/v1/admin/company/' . $toUpdate->id, [
            'name' => 'NewName'
        ])->seeJson([
            'name' => 'NewName'
        ]);

        $this->assertResponseStatus(202);
        $this->seeInDatabase('companies', [
            'name' => 'NewName'
        ]);
        $this->notSeeInDatabase('companies', [
            'name' => 'abc0'
        ]);
    }

    public function testUpdateInvalidData() {
        $c = $this->setUpData();
        $this->put('/api/v1/admin/company/' . $c[0]->id, [
            'a' => 'b'
        ]);
        $this->assertResponseStatus(202); // TODO: Respond with a not-changed response.
    }

    public function testRemove() {
        $c = $this->setUpData();

        $this->delete('/api/v1/admin/company/' . $c[0]->id);
        $this->assertResponseStatus(202);
        $this->notSeeInDatabase('companies', [
            'name' => 'abc0'
        ]);
    }

    public function testRemoveNotFound() {
        $this->delete('/api/v1/admin/company/123');
        $this->assertResponseStatus(404);
    }

}
