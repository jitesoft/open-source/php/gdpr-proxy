<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  UserControllerTest.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace App\Tests\Http\Controllers\Api\Admin;
use Jitesoft\GdprProxy\Models\User;
use TestCase;

/**
 * UserControllerTest
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class UserControllerTest extends TestCase {

    public function setUp() {
        parent::setUp();
        $this->withoutMiddleware();
    }

    private function setUpData() {
        $users = [];
        for ($i=0;$i<3;$i++) {
            $user = new User([
                'name' => 'test' . $i,
                'email' => 'test' . $i . '@email.com',
                'password' => 'asddsad'
            ]);
            $user->save();
            $users[] = $user;
        }
        return $users;
    }

    public function testGet() {
        $users = $this->setUpData();

        $this->get('/api/v1/admin/user/' . $users[0]->id)->seeJson([
            'name' => 'test0',
            'email' => 'test0@email.com'
        ]);
        $this->assertResponseOk();
    }

    public function testGetNotFound() {
        $this->get('/api/v1/admin/user/1');
        $this->assertResponseStatus(404);
    }

    public function testAll() {
        $this->setUpData();

        $this->get('/api/v1/admin/users');
        $this->assertResponseOk();
        $json = json_decode($this->response->getContent(), true);
        $this->assertArraySubset([
            [
                'name' => 'test0',
                'email' => 'test0@email.com'
            ],
            [
                'name' => 'test1',
                'email' => 'test1@email.com'],
            [
                'name' => 'test2',
                'email' => 'test2@email.com']
        ], $json);
    }

    public function testCreate() {
        $this->setUpData();
        $this->post('/api/v1/admin/user', [
            'name' => 'TestName',
            'email' => 'testname@example.com',
            'password' => 'abc123abc123'
        ])->seeJson([
            'name' => 'TestName',
            'email' => 'testname@example.com'
        ]);
        $this->assertResponseStatus(201);
        $this->seeInDatabase('users', [
            'name' => 'TestName'
        ]);
    }

    public function testCreateInvalidData() {
        $this->setUpData();
        $this->post('/api/v1/admin/user', [
            'name' => 'TestName',
            'email' => 'testname@example.com',
        ]);
        $this->assertResponseStatus(400);
    }

    public function testUpdate() {
        $users = $this->setUpData();

        $oldName = $users[0]->name;
        $this->put('/api/v1/admin/user/' . $users[0]->id, [
            'name' => 'NewName'
        ]);

        $this->assertResponseStatus(202);
        $this->seeInDatabase('users', [
            'name' => 'NewName'
        ]);
        $this->notSeeInDatabase('users', [
            'name' => $oldName
        ]);
    }

    public function testUpdateInvalidData() {
        $users = $this->setUpData();
        $this->put('/api/v1/admin/user/' . $users[0]->id, [
            'name' => 'NewName'
        ]);

        $this->assertResponseStatus(202); // TODO: Should return not changed response.
    }

    public function testRemove() {
        $users      = $this->setUpData();
        $idToRemove = $users[0]->id;

        $this->delete('/api/v1/admin/user/' . $idToRemove);
        $this->assertResponseStatus(202);
        $this->notSeeInDatabase('users', [
            'id' => $idToRemove
        ]);
    }

    public function testRemoveNotFound() {
        $this->delete('api/v1/admin/user/1');
        $this->assertResponseStatus(404);
    }

}
