<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  ConsumerServiceTest.php - Part of the gdpr-proxy project.

  © - Jitesoft 2018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
namespace App\Tests\Services;

use Jitesoft\Exceptions\Database\Entity\EntityException;
use Jitesoft\Exceptions\Security\AuthenticationException;
use Jitesoft\GdprProxy\Authentication\BasicAuth;
use Jitesoft\GdprProxy\Contracts\ConsumerServiceInterface;
use Jitesoft\GdprProxy\Models\Company;
use TestCase;

/**
 * ConsumerServiceTest
 * @author Johannes Tegnér <johannes@jitesoft.com>
 * @version 1.0.0
 */
class ConsumerServiceTest extends TestCase {

    /** @var ConsumerServiceInterface */
    private $service;

    public function setUp() {
        parent::setUp();

        $this->service = app(ConsumerServiceInterface::class);
    }

    public function testGetConsumerNotFound() {
        $this->expectException(EntityException::class);
        $this->service->getConsumer(1);
    }

    public function testGetConsumerFound() {
        $c1 = new Company(['name' => 'abc']);
        $c2 = new Company(['name' => 'efg']);
        $c1->save();
        $c2->save();

        $test = Company::find($c2->id);
        $out  = $this->service->getConsumer($c2->id);

        $this->assertEquals($test, $out);
    }

    public function testResolveAuthenticationTypeInvalidType() {
        $this->expectException(AuthenticationException::class);

        $this->service->resolveAuthenticationType('asaaa', new Company());
    }

    public function testResolveAuthenticationTypeBadData() {
        $this->expectException(AuthenticationException::class);

        $company = new Company(['auth_data' => [
            'aaaa'
        ]]);

        $this->service->resolveAuthenticationType('asaaa', $company);

    }
    public function testResolveAuthenticationTypeSuccess() {
        $company = new Company();

        $authData = new BasicAuth(['username' => 'abc', 'password' => 'abc']);
        $company->auth_data = $authData;

        $out = $this->service->resolveAuthenticationType(BasicAuth::TYPE, $company);
        $this->assertInstanceOf(BasicAuth::class, $out);
    }

    public function testCreateAuthenticationDataInvalidType() {
        $this->expectException(AuthenticationException::class);

        $this->service->createAuthenticationData('abc', []);
    }

    public function testCreateAuthenticationDataInvalidData() {

        $this->expectException(AuthenticationException::class);

        $this->service->createAuthenticationData(BasicAuth::TYPE, [
            'aaa' => 'bbb'
        ]);

    }

    public function testCreateAuthenticationDataValidData() {
        $out = $this->service->createAuthenticationData(BasicAuth::TYPE, [
            'username' => 'abc',
            'password' => 'abc'
        ]);
        $this->assertInstanceOf(BasicAuth::class, $out);
    }

}
