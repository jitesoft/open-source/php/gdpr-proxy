FROM jitesoft/composer as build
WORKDIR /app
COPY ./ /app
RUN composer install --no-progress --no-dev

FROM busybox
COPY --from=build /app /var/www/html
